source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use pg as database wrapper to postgresql ...
gem 'pg'

gem 'autoprefixer-rails'

gem 'cocoon'
gem 'simple_form'
gem 'chosen-rails'
gem 'paperclip', '~> 4.1'
gem 'aws-sdk', '~> 1.5.7'
gem 'kaminari'
gem 'ckeditor'
gem 'rmagick'

gem 'devise'
gem 'angular_rails_csrf'
gem 'omniauth'
gem 'omniauth-facebook', '~> 2.0.1'
gem 'fbgraph'
gem 'omniauth-google-oauth2'
gem 'google-id-token', git: 'https://github.com/Nerian/google-id-token'
gem 'geocoder'

gem 'activeadmin', git: 'https://github.com/activeadmin/activeadmin' 
gem 'ffaker', git: 'https://github.com/ffaker/ffaker'
gem 'faraday'
gem 'cancancan'
gem 'activeadmin_hstore_editor', git: 'https://github.com/wild5r/activeadmin_hstore_editor'
gem 'has_scope'

gem 'whenever', :require => false
gem 'delayed_job_active_record'
gem 'daemons'

gem 'capistrano-rails'
gem 'capistrano-rvm'
gem 'capistrano-passenger'

gem 'bower-rails'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'jquery-validation-rails'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'sdoc', '~> 0.4.0' # bundle exec rake doc:rails generates the API under doc/api.
  gem 'pry'
  gem 'pry-byebug'
  
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'simplecov', require: false
  gem 'simplecov-rcov', require: false
  gem 'capybara'

  gem 'better_errors'
  gem 'binding_of_caller'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  #gem 'spring'
  #gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
#gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
